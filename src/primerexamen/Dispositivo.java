/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primerexamen;

/**
 *
 * @author USUARIO
 */
public class Dispositivo extends Reservacion {
    private String Tipo;
    private String Modelo;
    private String Marca;

    public String getTipo() {
        return Tipo;
    }

    public void setTipo(String Tipo) {
        this.Tipo = Tipo;
    }

    public String getModelo() {
        return Modelo;
    }

    public void setModelo(String Modelo) {
        this.Modelo = Modelo;
    }

    public String getMarca() {
        return Marca;
    }

    public void setMarca(String Marca) {
        this.Marca = Marca;
    }

    @Override
    public String Imprimir() {
        return this.getTipo() + " " +
               this.getModelo() + " " +
               this.getMarca();
    }
}