/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primerexamen;

/**
 *
 * @author USUARIO
 */
public class Audifono extends Dispositivo{
    private String Alambricos;
    private String Inalambricos;

    public String getAlambricos() {
        return Alambricos;
    }

    public void setAlambricos(String Alambricos) {
        this.Alambricos = Alambricos;
    }

    public String getInalambricos() {
        return Inalambricos;
    }

    public void setInalambricos(String Inalambricos) {
        this.Inalambricos = Inalambricos;
    }
}
