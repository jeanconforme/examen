/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primerexamen;

/**
 *
 * @author USUARIO
 */
public class Laptop extends Dispositivo{
    
    private String LineaProfesional;
    private String LineaEntretenimiento;
   
    public String getLineaProfesional() {
        return LineaProfesional;
    }

    public void setLineaProfesional(String LineaProfesional) {
        this.LineaProfesional = LineaProfesional;
    }

    public String getLineaEntretenimiento() {
        return LineaEntretenimiento;
    }

    public void setLineaEntretenimiento(String LineaEntretenimiento) {
        this.LineaEntretenimiento = LineaEntretenimiento;
    }
}
